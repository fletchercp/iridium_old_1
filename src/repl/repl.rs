use parser::iris;
use std::io;
use std::io::prelude::*;
use vm::program::Program;
use vm::vm::VM;
use parser::register_allocator::RegisterAllocator;
use compiler::compiler::Compiler;
use parser::preprocessor;
use vm::function::Function;
/// REPL is the interactive shell for Iris
pub struct REPL {
    pub vm: VM,
    pub ra: RegisterAllocator,
}

impl REPL {
    pub fn new() -> REPL {
        REPL{
            vm: VM::new(),
            ra: RegisterAllocator::new(),
        }
    }

    pub fn start(&mut self) {
        println!("Welcome to Iridium! Let's be productive!");
        self.vm.set_program(Program::new(String::from("repl")));
        let mut compiler = Compiler::new(None);
        loop {
            let mut function_input = false;
            print!(">>> ");
            io::stdout().flush().unwrap();
            let mut input = String::new();
            match io::stdin().read_line(&mut input) {
                Ok(_) => {
                    match input.as_ref() {
                        "show registers\n" => {
                            self.vm.print_registers();
                            continue;
                        },
                        "show symbols\n" => {
                            self.vm.print_symbols();
                            continue;
                        },
                        "show heap\n" => {
                            self.vm.print_heap();
                            continue;
                        },
                        "show functions\n" => {
                            self.vm.print_functions();
                            continue;
                        }
                        _ => {},
                    }

                    let processed: Vec<String>;
                    if input.contains(":") {
                        if input.contains("def") {
                            function_input = true
                        }
                        let mut multiline = String::from(input.clone());
                        let mut done = false;
                        let mut lines = Vec::<String>::new();
                        lines.push(input.clone());
                        while !done {
                            let mut multiline_input = String::new();
                            print!("... ");
                            io::stdout().flush().ok().expect("Could not flush stdout");
                            match io::stdin().read_line(&mut multiline_input) {
                                Ok(_) => {
                                    match multiline_input.as_ref() {
                                        "\n" => {
                                            done = true;
                                            continue;
                                        },
                                        _ => {
                                            lines.push(multiline_input.clone());
                                            continue
                                        }
                                    }
                                },
                                Err(e) => {
                                    println!("Error capturing multiline input: {:?}", e);
                                    done = true;
                                    continue
                                },
                            }
                        }
                        processed = preprocessor::preprocess(lines);
                    } else {
                        let mut lines = Vec::<String>::new();
                        lines.push(input.clone());
                        processed = preprocessor::preprocess(lines);
                    }
                    let mut joined = processed.join("");
                    let result = iris::parse_Program(&joined);

                    match result {
                        Ok(parsed) => {
                            compiler.p = Some(parsed);
                            let mut instructions = compiler.compile();
                            let new_num = instructions.len();
                            if function_input == true {
                                let function_name = instructions[0].operands[0].clone().identifier.unwrap();
                                let store_instruction = instructions.remove(0);
                                match self.vm.program {
                                    Some(ref mut p) => {
                                        let new_function = Function::new();
                                        p.functions.insert(function_name.clone(), new_function);
                                        p.add_instructions_to_function(function_name, &mut instructions);
                                    },
                                    None => {},
                                }
                            } else {
                                match self.vm.program {
                                    Some(ref mut p) => {
                                        p.add_instructions_to_current_function(&mut instructions);
                                    },
                                    None => {},
                                }
                                for _ in 0..new_num {
                                    self.vm.execute_one();
                                }
                            }
                            self.vm.print_last_result();
                        },
                        Err(e) => {
                            println!("Error parsing expression: {:?}", e);
                        }
                    }
                }
                Err(error) => println!("error: {}", error),
            }
        }
    }
}
