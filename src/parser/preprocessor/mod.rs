/// This does some preprocessing of text to handle whitespace indentation

pub fn preprocess(text: Vec<String>) -> Vec<String> {
    let mut stack = Vec::<i64>::new();
    let mut result = Vec::<String>::new();
    stack.push(0);
    result.push(String::from("{"));
    for l in &text {
        let tab_count = l.matches("\t").count() as i64;
        if tab_count > *(stack.last().unwrap()) {
            result.push(String::from("{"));
            stack.push(tab_count);
        } else if tab_count < *(stack.last().unwrap()) {
            result.push(String::from("}"));
            stack.pop();
        }
        let trimmed = l.replace("\t", "");
        let trimmed = l.replace("\n", "");
        result.push(String::from(trimmed));
    }
    while let Some(_) = stack.pop() {
        result.push(String::from("}"));
    }
    result

}