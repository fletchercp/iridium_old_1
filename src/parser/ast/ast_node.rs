use std::collections::HashMap;
use parser::register_allocator::RegisterAllocator;
use compiler::symbol::Symbol;
use vm::instruction::Instruction;

pub trait ASTNode {
    fn compile(&self, instructions: &mut Vec<Instruction>, ra: &mut RegisterAllocator, st: &mut HashMap<String, Symbol>) -> usize;
}