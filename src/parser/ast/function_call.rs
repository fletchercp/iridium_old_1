use parser::ast::function_body::FunctionBody;
use parser::ast::function_header::FunctionHeader;
use parser::ast::ast_node::ASTNode;
use compiler::symbol::Symbol;
use compiler::symbol;
use vm::operand::Operand;
use parser::register_allocator::RegisterAllocator;
use std::collections::HashMap;
use vm::instruction::Instruction;
use parser::ast::identifier::Identifier;
#[derive(Debug, Clone)]
pub struct FunctionCall {
    pub id: Identifier,
    pub args: Vec<Identifier>,
}

impl FunctionCall {
    pub fn new(id: Identifier, args: Vec<Identifier>) -> FunctionCall {
        FunctionCall {
            id: id,
            args: args,
        }
    }
}

impl ASTNode for FunctionCall {
    fn compile(&self, instructions: &mut Vec<Instruction>, ra: &mut RegisterAllocator, st: &mut HashMap<String, Symbol>) -> usize {
        let mut call_operand = Operand::new(0, 0);
        let mut args = Vec::<Operand>::new();
        for a in &self.args {
            let mut operand = Operand::new(0, 0);
            operand.identifier = Some(a.id.clone());
            args.push(operand);
        }
        let mut call_instruction = Instruction::new_from_call(args);
        call_operand.identifier = Some(self.id.id.clone().replace("(", ""));
        call_instruction.operands.insert(0, call_operand);
        instructions.insert(0, call_instruction);
        0
    }
}