use std::collections::HashMap;
use parser::register_allocator::RegisterAllocator;
use parser::ast::ast_node::ASTNode;
use parser::ast::identifier::Identifier;
use parser::ast::expression::Expression;
use parser::ast;
use compiler::symbol::Symbol;
use parser::ast::operator::Operator;
use compiler::symbol;
use parser;
use vm::instruction::Instruction;


#[derive(Debug, Clone)]
pub struct Assignment {
    pub identifier: Identifier,
    pub expression: Box<Expression>,
}

impl Assignment {
    pub fn new(identifier: Identifier, expression: Box<Expression>) -> Box<Assignment> {
        Box::new(Assignment {
            identifier: identifier,
            expression: expression,
        })
    }
}

impl ASTNode for Assignment {
    fn compile(&self, instructions: &mut Vec<Instruction>, ra: &mut RegisterAllocator, st: &mut HashMap<String, symbol::Symbol>) -> usize {
        let value = self.expression.compile(instructions, ra, st);
        let new_symbol: Symbol;
        match self.expression.data_type.clone() {
            Some(st) => {
                match st {
                    parser::ast::types::DataType::Boolean => {
                        new_symbol = Symbol::new(0, value, symbol::SymbolType::Boolean);
                    },
                    parser::ast::types::DataType::Integer => {
                        new_symbol = Symbol::new(0, value, symbol::SymbolType::Integer);
                    },
                    parser::ast::types::DataType::List => {
                        new_symbol = Symbol::new(0, value, symbol::SymbolType::List);
                    },
                    parser::ast::types::DataType::IRString => {
                        new_symbol = Symbol::new(0, value, symbol::SymbolType::IRString);
                    }
                }
            },
            None => {
                // If here, the expression has both a left and a right with potentially different data types, which means we need to check operators
                match self.expression.operator.clone() {
                    Some(op) => {
                        match op  {
                            // TODO: We need to add the other operators
                            Operator::Add => {
                                // TODO: We need to check to make sure the types are able to be added
                                new_symbol = Symbol::new(0, value, symbol::SymbolType::Integer);
                            },
                            _ => {
                                // TODO: We should not assume everything is an Integer, obviously
                                new_symbol = Symbol::new(0, value, symbol::SymbolType::Integer);
                            }
                        }
                    },
                    None => {
                        return 0;
                    },
                }
            },
        }


        let mut store_instruction = Instruction::new_from_sto(value, symbol::symbol_type_to_int(new_symbol.symbol_type.clone()), self.identifier.id.clone());
        match new_symbol.symbol_type {
            symbol::SymbolType::Boolean => {
                store_instruction.operands[0].register = self.expression.boolean_value.clone().unwrap().value as usize;
            },
            symbol::SymbolType::List => {
                store_instruction.operands[0].strings = self.expression.list.clone().unwrap().value.clone();
            },
            symbol::SymbolType::IRString => {

                store_instruction.operands[0].strings.push(self.expression.irstring.clone().unwrap().value.clone());
            },
            _ => {},
        }

        instructions.push(store_instruction);
        st.insert(self.identifier.id.clone(), new_symbol);
        value
    }
}
