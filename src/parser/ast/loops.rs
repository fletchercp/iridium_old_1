use parser::ast::identifier::Identifier;
use parser::ast::block::Block;
use parser::ast::ast_node::ASTNode;
use vm::instruction::Instruction;
use parser::register_allocator::RegisterAllocator;
use std::collections::HashMap;
use compiler::symbol::Symbol;

#[derive(Debug, Clone)]
pub struct ForLoop {
    pub identifier: Identifier,
    pub collection: Identifier,
    block: Block,
}

impl ForLoop {
    pub fn new(i: Identifier, c: Identifier, b: Block) -> ForLoop {
        ForLoop {
            identifier: i,
            collection: c,
            block: b,
        }
    }
}

impl ASTNode for ForLoop {
    fn compile(&self, instructions: &mut Vec<Instruction>, ra: &mut RegisterAllocator, st: &mut HashMap<String, Symbol>) -> usize {
        self.block.compile(instructions, ra, st);
        0
    }
}