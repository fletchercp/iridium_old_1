use std::collections::HashMap;
use parser::register_allocator::RegisterAllocator;
use parser::ast::ast_node::ASTNode;
use compiler::symbol::Symbol;
use vm::instruction::Instruction;

#[derive(Debug, Clone)]
pub struct IRString {
    pub value: String,
}

impl IRString {
    pub fn new(value: String) -> IRString {
        IRString {
            value: value,
        }
    }
}

impl ASTNode for IRString {
    fn compile(&self, instructions: &mut Vec<Instruction>, _: &mut RegisterAllocator, st: &mut HashMap<String, Symbol>) -> usize {
        0
    }
}
