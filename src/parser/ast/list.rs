use parser::ast::ast_node::ASTNode;
use compiler::symbol::Symbol;
use compiler::symbol;
use vm::operand::Operand;
use parser::register_allocator::RegisterAllocator;
use std::collections::HashMap;
use vm::instruction::Instruction;

#[derive(Debug, Clone)]
pub struct List {
    pub value: Vec<String>,
}

impl List {
    pub fn new(elements: String) -> List {
        let mut new_list = List {
            value: Vec::<String>::new(),
        };
        let mut elements = elements.replace("[", "");
        let mut elements = elements.replace("]", "");
        let split_elements: Vec<&str> = elements.split(',').collect();
        for elem in &split_elements {
            let tmp = elem.replace(" ", "");
            new_list.value.push(tmp.to_string());
        }
        new_list
    }
}


impl ASTNode for List {
    fn compile(&self, instructions: &mut Vec<Instruction>, ra: &mut RegisterAllocator, st: &mut HashMap<String, Symbol>) -> usize {
        0   
    }
}