use std::collections::HashMap;
use parser::register_allocator::RegisterAllocator;
use parser::ast::ast_node::ASTNode;
use parser::ast::identifier::Identifier;
use parser::ast::list::List;
use compiler::symbol::Symbol;
use vm::instruction::Instruction;
use parser::ast::operator::Operator;
use parser::ast::num::Num;
use parser::ast::irstring::IRString;
use parser::ast::types::DataType;
use parser::ast::boolean::Boolean;
use vm::opcode;
use vm::operand::Operand;
use parser::ast::function_call::FunctionCall;

#[derive(Debug, Clone)]
pub struct Expression {
    pub operator: Option<Operator>,
    pub value: Option<Num>,
    pub boolean_value: Option<Boolean>,
    pub identifier: Option<Identifier>,
    pub left: Option<Box<Expression>>,
    pub right: Option<Box<Expression>>,
    pub data_type: Option<DataType>,
    pub function_call: Option<Box<FunctionCall>>,
    pub list: Option<List>,
    pub irstring: Option<IRString>,
}

impl Expression {
    pub fn new_from_triple(l: Box<Expression>, o: Operator, r: Box<Expression>) -> Box<Expression> {
        Box::new(Expression {
            operator: Some(o),
            left: Some(l),
            right: Some(r),
            value: None,
            identifier: None,
            data_type: None,
            boolean_value: None,
            function_call: None,
            list: None,
            irstring: None,
        })
    }

    pub fn new_from_num(n: Num) -> Box<Expression> {
        Box::new(Expression {
            operator: None,
            left: None,
            right: None,
            value: Some(n),
            identifier: None,
            data_type: Some(DataType::Integer),
            boolean_value: None,
            function_call: None,
            list: None,
            irstring: None,
        })
    }

    pub fn new_from_bool(b: Boolean) -> Box<Expression> {
        Box::new(Expression {
            operator: None,
            left: None,
            right: None,
            value: None,
            identifier: None,
            data_type: Some(DataType::Boolean),
            boolean_value: Some(b),
            function_call: None,
            list: None,
            irstring: None,
        })
    }

    pub fn new_from_identifier(i: Identifier) -> Box<Expression> {
        Box::new(Expression {
            operator: None,
            left: None,
            right: None,
            identifier: Some(i),
            value: None,
            data_type: None,
            boolean_value: None,
            function_call: None,
            list: None,
            irstring: None,
        })
    }

    pub fn new_from_list(l: List) -> Box<Expression> {
        Box::new(Expression {
            operator: None,
            left: None,
            right: None,
            identifier: None,
            value: None,
            data_type: Some(DataType::List),
            boolean_value: None,
            function_call: None,
            list: Some(l),
            irstring: None,
        })
    }

    pub fn new_from_irstring(irs: IRString) -> Box<Expression> {
        println!("Creating new irstring");
        Box::new(Expression {
            operator: None,
            left: None,
            right: None,
            identifier: None,
            value: None,
            data_type: Some(DataType::IRString),
            boolean_value: None,
            function_call: None,
            list: None,
            irstring: Some(irs),
        })
    }

    pub fn has_identifier(&self) -> bool {
        match self.identifier {
            Some(_) => {
                true
            },
            None => {
                false
            },
        }
    }

    pub fn clone_identifier(&self) -> Option<String> {
        match self.identifier {
            Some(ref v) => {
                Some(v.id.clone())
            },
            None => {
                None
            },
        }
    }
}

impl ASTNode for Expression {
    fn compile(&self, instructions: &mut Vec<Instruction>, ra: &mut RegisterAllocator, st: &mut HashMap<String, Symbol>) -> usize {
        match self.value {
            // If there is a value, then we don't need to do an operation since it is just a Num
            Some(ref v) => {
                let r1 = ra.allocate_register();
                let ins1 = Instruction::new_from_lod(v.value, r1);
                instructions.insert(0, ins1);
                return r1;
            },
            None => {},
        }

        match self.boolean_value {
            Some(ref v) => {
                return v.value as usize;
            },
            None => {},
        }

        match self.identifier {
            Some(ref i) => {
                i.compile(instructions, ra, st);
                match st.get(&i.id.clone()) {
                    Some(register) => {
                        // If it is an identifier, we need to add an instructinon to load the value into a register
                        return register.address;
                    },
                    None => {},
                }
            },
            None => {},
        }

        match self.function_call {
            Some(ref fc) => {
                fc.compile(instructions, ra, st);
            },
            None => {},
        }

        match self.list {
            Some(ref l) => {
                l.compile(instructions, ra, st);
            },
            None => {},
        }

        let mut r1: usize = 0;
        let mut r2: usize = 0;

        match self.left {
            Some(ref v) => {
                r1 = v.compile(instructions, ra, st);
            },
            None => {},
        }

        match self.right {
            Some(ref v) => {
                r2 = v.compile(instructions, ra, st);
            },
            None => {},
        }

        let result_register: usize = ra.allocate_register();
        match self.operator {
            Some(operator) => {
                match operator {
                    Operator::Add => {
                        let mut new_instruction = Instruction::new(opcode::ADD).unwrap();
                        let mut op1 = Operand::new(0, 0);
                        let mut op2 = Operand::new(0, 0);
                        let mut op3 = Operand::new(0, 0);

                        match self.left {
                            Some(ref v) => {
                                if v.has_identifier() == true {
                                    op1.identifier = v.clone_identifier();
                                } else {
                                    op1.register = r1;
                                }
                            },
                            None => {

                            },
                        }

                        match self.right {
                            Some(ref v) => {
                                if v.has_identifier() == true {
                                    op2.identifier = v.clone_identifier();
                                } else {
                                    op2.register = r2;
                                }
                            },
                            None => {

                            },
                        }

                        op3.register = result_register;
                        new_instruction.add_operand(op1);
                        new_instruction.add_operand(op2);
                        new_instruction.add_operand(op3);
                        instructions.push(new_instruction);
                    },
                    Operator::Sub => {
                        let ins3 = Instruction::new_from_sub(r1, r2, result_register);
                        instructions.push(ins3);
                    },
                    Operator::Mul => {
                        let ins3 = Instruction::new_from_mul(r1, r2, result_register);
                        instructions.push(ins3);
                    },
                    Operator::Div => {
                        let ins3 = Instruction::new_from_div(r1, r2, result_register);
                        instructions.push(ins3);
                    },
                    _ => {
                        // TODO: Fill this in
                    },
                }
            },
            None => {
                return 0;
            }
        }
        match self.left {
            Some(ref l) => {
                match l.value {
                    Some(_) => {
                        ra.free_register(r1)
                    },
                    None => {},
                }
            },
            None => {},
        }

        match self.right {
            Some(ref r) => {
                match r.value {
                    Some(_) => {
                        ra.free_register(r2);
                    },
                    None => {},
                }
            },
            None => {},
        }

        result_register
    }
}
