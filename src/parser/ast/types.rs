#[derive(Clone, Debug)]
pub enum DataType {
    Integer,
    Boolean,
    List,
    IRString,
}
