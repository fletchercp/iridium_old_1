use std::collections::HashMap;
use parser::ast::block::Block;
use parser::register_allocator::RegisterAllocator;
use compiler::symbol::Symbol;
use vm::instruction::Instruction;
use parser::ast::ast_node::ASTNode;


#[derive(Debug, Clone)]
pub struct Program {
    block: Block,
}

impl Program {
    pub fn new(block: Block) -> Program {
        Program{
          block: block,
        }
    }

    pub fn compile_program(&self, ra: &mut RegisterAllocator, st: &mut HashMap<String, Symbol>) -> Vec<Instruction> {
        let mut instructions = Vec::<Instruction>::new();
        self.block.compile(&mut instructions, ra, st);
        println!("Instructions: {:?}", instructions);
        instructions
    }
}