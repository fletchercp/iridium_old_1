
#[derive(Copy, Clone, Debug)]
/// A mathematical operator
pub enum Operator {
    Add,
    Sub,
    Mul,
    Div,
    Gt,
    Lt,
    Equ,
    Neq,
    Gte,
    Lte,
    And,
    Or,
    Not
}