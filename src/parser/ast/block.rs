use std::collections::HashMap;
use parser::register_allocator::RegisterAllocator;
use parser::ast::ast_node::ASTNode;
use compiler::symbol::Symbol;
use vm::instruction::Instruction;
use parser::ast::statement::Statement;
#[derive(Debug, Clone)]
pub struct Block {
    statements: Vec<Statement>,
}

impl Block {
    pub fn new(statements: Vec<Statement>) -> Block {
        Block {
            statements: statements,
        }
    }
}

impl ASTNode for Block {
    fn compile(&self, instructions: &mut Vec<Instruction>, ra: &mut RegisterAllocator, st: &mut HashMap<String, Symbol>) -> usize {
        for s in &self.statements {
            s.compile(instructions, ra, st);
        }
        return 0;
    }
}