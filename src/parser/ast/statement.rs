use std::collections::HashMap;
use parser::register_allocator::RegisterAllocator;
use parser::ast::ast_node::ASTNode;
use parser::ast::identifier::Identifier;
use parser::ast::expression::Expression;
use compiler::symbol::Symbol;
use vm::instruction::Instruction;
use parser::ast::function::Function;
use parser::ast::function_call::FunctionCall;
use parser::ast::block::Block;
use parser::ast::assignment::Assignment;
use parser::ast::loops::ForLoop;
use parser::ast::num::Num;
use vm::opcode;
use vm::operand;

#[derive(Debug, Clone)]
pub struct Statement {
    expression: Option<Box<Expression>>,
    function_call: Option<FunctionCall>,
    assignment: Option<Box<Assignment>>,
    num: Option<Num>,
    identifier: Option<Identifier>,
    function: Option<Function>,
    block: Option<Block>,
    forloop: Option<ForLoop>,
}

impl Statement {
    pub fn new_from_expression(e: Box<Expression>) -> Statement {
        Statement {
            expression: Some(e),
            function_call: None,
            block: None,
            num: None,
            assignment: None,
            identifier: None,
            function: None,
            forloop: None,
        }
    }

    pub fn new_from_assignment(a: Box<Assignment>) -> Statement {
        Statement {
            expression: None,
            function_call: None,
            block: None,
            num: None,
            assignment: Some(a),
            identifier: None,
            function: None,
            forloop: None,
        }
    }

    pub fn new_from_function(f: Function) -> Statement {
        Statement {
            function: Some(f),
            function_call: None,
            block: None,
            expression: None,
            num: None,
            assignment: None,
            identifier: None,
            forloop: None,
        }
    }

    pub fn new_from_function_call(f: FunctionCall) -> Statement {
        Statement {
            function: None,
            function_call: Some(f),
            block: None,
            expression: None,
            num: None,
            assignment: None,
            identifier: None,
            forloop: None,
        }
    }

    pub fn new_from_block(b: Block) -> Statement {
        Statement {
            function: None,
            function_call: None,
            block: Some(b),
            expression: None,
            num: None,
            assignment: None,
            identifier: None,
            forloop: None,
        }
    }

    pub fn new_from_for_loop(l: ForLoop) -> Statement {
        Statement {
            function: None,
            function_call: None,
            block: None,
            expression: None,
            num: None,
            assignment: None,
            identifier: None,
            forloop: Some(l),
        }
    }
}

impl ASTNode for Statement {
    fn compile(&self, instructions: &mut Vec<Instruction>, ra: &mut RegisterAllocator, st: &mut HashMap<String, Symbol>) -> usize {
        match self.expression {
            Some(ref e) => {
                if e.left.is_none() && e.right.is_none() && e.identifier.is_some() {
                    /*
                    let mut ins = Instruction::new(opcode::PRI).unwrap();
                    let mut o1 = operand::Operand::new(0, 0);
                    o1.identifier = Some(e.identifier.clone().unwrap().id.clone());
                    ins.add_operand(o1);
                    instructions.push(ins);
                    */
                } else {
                    e.compile(instructions, ra, st);
                }
            },
            None => {},
        }

        match self.forloop {
            Some(ref e) => {
                let mut forl_instruction = Instruction::new(opcode::FORL).unwrap();

                // Store the collection ID
                let mut op1 = operand::Operand::new(0, 0);
                op1.identifier = Some(e.collection.id.clone());

                // Store the iterator variable ID
                let mut op2 = operand::Operand::new(0, 0);
                op2.identifier = Some(e.identifier.id.clone());

                forl_instruction.add_operand(op1);
                forl_instruction.add_operand(op2);

                instructions.push(forl_instruction);
                // Get current number of instructions
                let old_length = instructions.len();
                
                e.compile(instructions, ra, st);
                
                // Get new length of instructions after compiling the loop block
                let new_length = instructions.len();

                // Calculate how many lines the loop is, so we can jump back
                let diff = (new_length - old_length) as i64;

                // Create a NXT instruction to move the Iterator
                let mut nxt_instruction = Instruction::new(opcode::NXT).unwrap();
                let mut n1 = operand::Operand::new(0, 0);
                let mut n2 = operand::Operand::new(0, 0);
                n1.identifier = Some(e.collection.id.clone());
                n2.identifier = Some(e.identifier.id.clone());
                nxt_instruction.add_operand(n1);
                nxt_instruction.add_operand(n2);
                instructions.push(nxt_instruction);

                // Create an instruction to jump back to the top of the loop
                let mut jmpb_instruction = Instruction::new(opcode::JMPB).unwrap();
                let mut o1 = operand::Operand::new(0, 0);
                o1.value = diff - 1;
                jmpb_instruction.add_operand(o1);
                instructions.push(jmpb_instruction);
            },
            None => {},
        }

        match self.assignment {
            Some(ref e) => {
                e.compile(instructions, ra, st);
            },
            None => {},
        }

        match self.identifier {
            Some(ref e) => {
                e.compile(instructions, ra, st);
            },
            None => {},
        }

        match self.function {
            Some (ref f) => {
                f.compile(instructions, ra, st);
                // Need to add a RET instruction at the end of every function
                let ret_instruction = Instruction::new(opcode::RET).unwrap();
                instructions.push(ret_instruction);
            },
            None => {},
        }

        match self.function_call {
            Some (ref fc) => {
                fc.compile(instructions, ra, st);
            },
            None => {},
        }
        
        return 0;
    }
    
}
