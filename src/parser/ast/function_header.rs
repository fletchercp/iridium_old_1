use parser::ast::identifier::Identifier;
use compiler::symbol::Symbol;
use compiler::symbol;
use vm::operand::Operand;
use std::collections::HashMap;
use vm::instruction::Instruction;
use parser::register_allocator::RegisterAllocator;
use parser::ast::ast_node::ASTNode;

#[derive(Debug, Clone)]
pub struct FunctionHeader {
    pub id: Identifier,
    pub args: Vec<Identifier>,
}

impl FunctionHeader {
    pub fn new(id: Identifier, args: Vec<Identifier>) -> FunctionHeader {
        let new_id = id.id.clone().replace("(","");
        let mut fh = FunctionHeader {
            id: id,
            args: args,
        };
        fh.id.set_id(new_id);
        fh
    }
}

impl ASTNode for FunctionHeader {
    fn compile(&self, instructions: &mut Vec<Instruction>, ra: &mut RegisterAllocator, st: &mut HashMap<String, Symbol>) -> usize {
        // First we need to tell the VM to create the function on the heap
        0
    }
}

