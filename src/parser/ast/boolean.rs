#[derive(Clone, Debug)]
pub struct Boolean {
    pub value: bool,
}

impl Boolean {
    pub fn new(b: String) -> Boolean {
        let v: bool;
        if &b == "True" {
            v = true;
        } else {
            v = false;
        }
        Boolean {
            value: v,
        }
    }
}