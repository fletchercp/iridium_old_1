use std::collections::HashMap;
use parser::register_allocator::RegisterAllocator;
use parser::ast::ast_node::ASTNode;
use compiler::symbol::Symbol;
use vm::instruction::Instruction;

#[derive(Debug, Clone)]
pub struct Identifier {
    pub id: String,
}

impl Identifier {
    pub fn new(id: String) -> Identifier {
        Identifier {
            id: id,
        }
    }

    pub fn set_id(&mut self, id: String) {
        self.id = id;
    }
}

impl ASTNode for Identifier {
    fn compile(&self, instructions: &mut Vec<Instruction>, _: &mut RegisterAllocator, st: &mut HashMap<String, Symbol>) -> usize {
        match st.get(&self.id) {
            Some(register) => {
                register.address
            },
            None => {
                0
            },
        }
    }
}
