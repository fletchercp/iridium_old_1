#[derive(Debug, Clone)]
pub struct Num {
    pub value: i64,
}

impl Num {
    pub fn new(n: i64) -> Num {
        Num {
            value: n,
        }
    }
}
