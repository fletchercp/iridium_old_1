use parser::ast::function_body::FunctionBody;
use parser::ast::function_header::FunctionHeader;
use parser::ast::ast_node::ASTNode;
use compiler::symbol::Symbol;
use compiler::symbol;
use vm::operand::Operand;
use parser::register_allocator::RegisterAllocator;
use std::collections::HashMap;
use vm::instruction::Instruction;

#[derive(Debug, Clone)]
pub struct Function {
    pub header: FunctionHeader,
    pub body: FunctionBody,
}

impl Function {
    pub fn new(header: FunctionHeader, body: FunctionBody) -> Function {
        Function {
            header: header,
            body: body,
        }
    }
}

impl ASTNode for Function {
    fn compile(&self, instructions: &mut Vec<Instruction>, ra: &mut RegisterAllocator, st: &mut HashMap<String, Symbol>) -> usize {
        let mut store_instruction = Instruction::new_from_sto(self.header.args.len(), symbol::symbol_type_to_int(symbol::SymbolType::Function), self.header.id.id.clone());
        for a in &self.header.args {
            let mut operand = Operand::new(0, 0);
            operand.identifier = Some(a.id.clone());
            store_instruction.add_operand(operand);
        }
        
        let start_instruction = instructions.len() as i64;
        self.body.compile(instructions, ra, st);
        let end_instruction = instructions.len() as i64;
        let diff = end_instruction - start_instruction;
        let operand = Operand::new(ra.instruction_count as usize, ra.instruction_count + diff);
        store_instruction.add_operand(operand);
        instructions.insert(0, store_instruction);
        0
    }
}