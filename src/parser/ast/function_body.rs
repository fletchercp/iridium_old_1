use parser::ast::block::Block;
use parser::ast::ast_node::ASTNode;
use compiler::symbol::Symbol;
use std::collections::HashMap;
use vm::instruction::Instruction;
use parser::register_allocator::RegisterAllocator;
#[derive(Debug, Clone)]
pub struct FunctionBody {
    block: Block,
}

impl FunctionBody {
    pub fn new(b: Block) -> FunctionBody {
        FunctionBody {
            block: b,
        }
    }
}

impl ASTNode for FunctionBody {
    fn compile(&self, instructions: &mut Vec<Instruction>, ra: &mut RegisterAllocator, st: &mut HashMap<String, Symbol>) -> usize {
        self.block.compile(instructions, ra, st);
        0
    }
}
