use vm::vm::REGISTERS;

/// RegisterAllocator handles the allocation of registers
pub struct RegisterAllocator {
    free_registers: Vec<usize>,
    pub instruction_count: i64
}

impl RegisterAllocator {
    pub fn new() -> RegisterAllocator {
        let mut ra = RegisterAllocator{
            free_registers: Vec::new(),
            instruction_count: 0,
        };
        
        for i in 0..REGISTERS {
            ra.free_registers.push(i);
        }
        ra
    }

    pub fn allocate_register(&mut self) -> usize {
        self.free_registers.pop().unwrap()
        
    }

    pub fn free_register(&mut self, r: usize) {
        self.free_registers.push(r);
    }
}