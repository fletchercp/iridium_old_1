#[derive(Debug, Clone)]
/// enum for the various things a symbol can be (variable, function, etc)
pub enum SymbolType {
    Integer,
    Boolean,
    Function,
    List,
    IRString,
    Invalid,
}

#[derive(Debug, Clone)]
/// Symbol tracks the various information about a symbol the compiler has found
pub struct Symbol {
    // scope tracks the symbol scope so we know when it should be expired
    pub scope: usize,
    // tracks the address where it is stored
    pub address: usize,
    // tracks what type of symbol this is (variable, function, etc)
    pub symbol_type: SymbolType,
}

impl Symbol {
    /// Creates and returns a new symbol
    pub fn new(scope: usize, address: usize, symbol_type: SymbolType) -> Symbol {
        Symbol {
            scope: scope,
            address: address,
            symbol_type: symbol_type,
        }
    }
}

pub fn int_to_symbol_type(i: usize) -> SymbolType {
    match i {
        0 => SymbolType::Integer,
        1 => SymbolType::Boolean,
        2 => SymbolType::Function,
        3 => SymbolType::List,
        4 => SymbolType::IRString,
        _ => SymbolType::Invalid,
    }
}

pub fn symbol_type_to_int(st: SymbolType) -> usize {
    match st {
        SymbolType::Integer  => 0,
        SymbolType::Boolean  => 1,
        SymbolType::Function => 2,
        SymbolType::List     => 3,
        SymbolType::IRString => 4,
        SymbolType::Invalid  => 5,
    }
}
