use parser::ast::program::Program;
use parser::ast::expression::Expression;
use parser::ast::statement::Statement;
use parser::ast::block::Block;
use vm::instruction::Instruction;
use parser::register_allocator::RegisterAllocator;
use std::collections::HashMap;
use compiler::symbol::Symbol;
use parser::ast::ast_node::ASTNode;

/// Compiler takes the AST and compiles it to Iridium bytecode
pub struct Compiler {
    pub p: Option<Program>,
    pub ra: Option<RegisterAllocator>,
    /// st is the Symbol Table. It maps identifiers to 
    pub st: HashMap<String, Symbol>,
}

impl Compiler {
    pub fn new(p: Option<Program>) -> Compiler {
        Compiler {
            p: p,
            ra: Some(RegisterAllocator::new()),
            st: HashMap::new(),   
        }
    }

    pub fn print_symbols(&self) {
        println!("-------------------");
        println!("SYMBOLS");
        println!("-------------------");
        for s in &self.st {
            println!("{:?}", s);
        }
        println!("-------------------");
    }

    pub fn compile_expression(&mut self, e: Expression) -> Vec<Instruction> {
        match self.ra {
            Some(ref mut reg_alloc) => {
                let mut instructions = Vec::<Instruction>::new();
                e.compile(&mut instructions, reg_alloc, &mut self.st);
                instructions
            },
            None => { return Vec::<Instruction>::new() },
        }

    }

    pub fn compile_statement(&mut self, s: Statement) -> Vec<Instruction> {
        match self.ra {
            Some(ref mut reg_alloc) => {
                let mut instructions = Vec::<Instruction>::new();
                s.compile(&mut instructions, reg_alloc, &mut self.st);
                instructions
            },
            None => { return Vec::<Instruction>::new() },
        }
    }

    pub fn compile_block(&mut self, b: Block) -> Vec<Instruction> {
        match self.ra {
            Some(ref mut reg_alloc) => {
                let mut instructions = Vec::<Instruction>::new();
                b.compile(&mut instructions, reg_alloc, &mut self.st);
                instructions
            },
            None => { return Vec::<Instruction>::new() },
        }
    }

    pub fn compile_program(&mut self, p: Program) -> Vec<Instruction> {
        match self.ra {
            Some(ref mut reg_alloc) => {
                let mut instructions = p.compile_program(reg_alloc, &mut self.st);
                instructions
            },
            None => { return Vec::<Instruction>::new() },
        }
    }

    pub fn compile(&mut self) -> Vec<Instruction> {
        match self.p {
            Some(ref mut program) => {
                match self.ra {
                    Some(ref mut reg_alloc) => {
                        let instructions = program.compile_program(reg_alloc, &mut self.st);

                        // TODO: This is such a hack to help do function calls int he REPL. Figure out a better way.
                        let mut ic = reg_alloc.instruction_count;
                        ic = ic + instructions.len() as i64;
                        reg_alloc.instruction_count = ic;

                        instructions
                    },
                    None => {
                        let empty: Vec<Instruction> = Vec::new();
                        empty
                    },
                }
                
            },
            None => {
                let empty: Vec<Instruction> = Vec::new();
                empty
            }
        }
        
    }
}