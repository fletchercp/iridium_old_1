pub const LOD: usize = 0;
pub const ADD: usize = 1;
pub const SUB: usize = 2;
pub const MUL: usize = 3;
pub const DIV: usize = 4;
pub const PRL: usize = 5;
pub const PRR: usize = 6;
pub const STO: usize = 7;
pub const ADDH: usize = 8;
pub const PRI: usize = 9;
pub const CALL: usize = 10;
pub const RET: usize = 11;
pub const LIST: usize = 12;
pub const FORL: usize = 13;
pub const JMPB: usize = 14;
pub const NXT: usize = 15;

#[derive(Debug, Clone)]
// Opcode is a specific Opcode the VM supports
pub struct Opcode {
    pub op_type: usize,
    pub text: String,
}

impl Opcode {
    pub fn new(op_type: usize) -> Option<Opcode> {
        let text = opcode_usize_to_text(op_type);
        if text == None {
            return None;
        }

        let new_opcode = Opcode {
            op_type: op_type,
            text: text.unwrap(),
        };

        Some(new_opcode)
    }
}

fn opcode_usize_to_text(t: usize) -> Option<String> {
    if t == LOD {
        return Some(String::from("LOD"));
    } else if t == PRL {
        return Some(String::from("PRL"));
    } else if t == ADD {
        return Some(String::from("ADD"));
    } else if t == SUB {
        return Some(String::from("SUB"));
    } else if t == DIV {
        return Some(String::from("DIV"));
    } else if t == MUL {
        return Some(String::from("MUL"));
    } else if t == PRR {
        return Some(String::from("PRR"));
    } else if t == STO {
        return Some(String::from("STO"));
    } else if t == ADDH {
        return Some(String::from("ADDH"));
    } else if t == PRI {
        return Some(String::from("PRI"));
    } else if t == CALL {
        return Some(String::from("CALL"));
    } else if t == RET {
        return Some(String::from("RET"));
    } else if t == LIST {
        return Some(String::from("LIST"));
    } else if t == FORL {
        return Some(String::from("FORL"));
    } else if t == JMPB {
        return Some(String::from("JMPB"));
    } else if t == NXT {
        return Some(String::from("NXT"));
    } else {
        return None;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_correct_type() {
        let oc = Opcode::new(ADD).unwrap();
        assert_eq!(oc.op_type, ADD);
    }

    #[test]
    fn test_incorrect_type() {
        let oc = Opcode::new(99999);
        match oc {
            None => assert!(true),
            Some(_) => assert!(false),
        };
    }
}
