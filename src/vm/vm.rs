use vm::register::Register;
use vm::program::Program;
use vm::instruction::Instruction;
use vm::opcode;
use vm::operand::Operand;
use vm::memory::heap::VMHeap;
use vm::types::*;
use vm;
use std::collections::HashMap;
use compiler;
use vm::types::object::Object;
use vm::function_pointer::FunctionPointer;
use std::thread;
use std::sync::{Arc, RwLock};
pub const REGISTERS: usize = 10;
/// The VM executes Iridium vytecode
pub struct VM {
    registers: Vec<Register>,
    // Special register that stores the last result that returned a value
    lr: Register,
    symbols: HashMap<String, usize>,
    objects: HashMap<String, Arc<RwLock<Box<Object>>>>,
    vm_heap: VMHeap,
    call_stack: Vec<FunctionPointer>,
    pub program: Option<Program>,
    pc: usize,
    current_function: String,
    looping: bool,
}

impl VM {
    pub fn new() -> VM {
        let mut new_vm = VM {
            registers: Vec::new(),
            lr: Register::new(1000, 0),
            vm_heap: VMHeap::new(),
            call_stack: Vec::new(),
            program: None,
            symbols: HashMap::new(),
            objects: HashMap::new(),
            current_function: String::from("main"),
            pc: 0,
            looping: false,
        };
        for _ in 0..REGISTERS {
            new_vm.add_register();
        }
        new_vm
    }

    fn add_register(&mut self) -> usize {
        let next_id = self.registers.len();
        let new_register = Register::new(next_id, 0);
        self.registers.push(new_register);
        next_id
    }

    pub fn print_registers(&self) {
        println!("-------------------");
        println!("REGISTERS");
        println!("-------------------");
        for r in &self.registers {
            println!("{:?} {:?}", r.id, r.value);
        }
        println!("-------------------");
    }

    pub fn print_symbols(&self) {
        println!("-------------------");
        println!("SYMBOLS");
        println!("-------------------");
        for s in &self.symbols {
            println!("{:?}", s);
        }
        println!("-------------------");
    }

    pub fn print_heap(&self) {
        println!("-------------------");
        println!("HEAP");
        println!("-------------------");
        for s in &self.vm_heap.storage {
            println!("{:?}({:?})", s.1.GetType(), s.1.Text());
        }
        println!("-------------------");
    }

    pub fn print_functions(&self) {
        println!("-------------------");
        println!("FUNCTIONS");
        println!("-------------------");
        match self.program {
            Some(ref p) => {
                p.print_functions();
            },
            None => {
                // TODO: Something here...
            },
        }
        println!("-------------------");
    }

    pub fn print_last_result(&self) {
        match self.lr.value {
            Some(v) => {
                println!("{:?}", v);
            },
            None => {},
        }

    }

    pub fn set_program(&mut self, p: Program) {
        self.program = Some(p);
    }

    /// Execute starts execution of the VM

    /*
    pub fn execute(&mut self) {
        loop {
            match self.program {
                Some(_) => {
                    self.execute_next_instruction();
                },
                None => {
                    let ten_millis = time::Duration::from_millis(10);
                    thread::sleep(ten_millis);
                    continue;
                }
            }
        }
    }
    */

    /// LOD loads an integer into a register
    fn lod(&mut self, ins: &Instruction) {
        let r1 = ins.operands[0].register;
        self.registers[r1].value = Some(ins.operands[0].value);
    }

    /// PRR prints the contents of a register
    fn prr(&mut self, ins: &Instruction) {
        let r1 = ins.operands[0].register;
        let value = self.registers[r1].value;
        println!("{:?}", value);
    }

    /// PRL prints the literal value in the first operand
    fn prl(&mut self, ins: &Instruction) {
        let r1 = ins.operands[0].value;
        println!("{:?}", r1);
    }

    fn identifier_exists(&self, id: String) -> bool {
        let v = self.symbols.get(&id);
        match v {
            Some(_) => {
                true
            },
            None => {
                false
            }
        }
    }

    fn get_i64_from_heap(&self, symbol_name: String) -> Option<i64> {
        let result = self.symbols.get(&symbol_name);
        match result {
            Some(address) => {
                let object = self.vm_heap.get(*address);
                match object {
                    Some(ref o) => {
                        Some(o.Text().parse::<i64>().unwrap())
                    },
                    None => {
                        println!("Object not found in heap!");
                        None
                    },
                }
            },
            None => {
                println!("Symbol not found!");
                None
            },
        }
    }

    pub fn execute_one(&mut self) {
        while self.looping == true {
            self.execute_next_instruction();
        }
        self.execute_next_instruction();
    }

    fn execute_next_instruction(&mut self) {
        match self.program.clone() {
                    Some(p) => {
                        let result = p.functions.get(&self.current_function);
                        match result {
                            Some(ni) => {
                                if ni.instructions.len() <= self.pc {
                                    return;
                                }
                                let next_instruction = &ni.instructions[self.pc];
                                match next_instruction.opcode.op_type {
                                    opcode::PRL => {
                                        self.prl(next_instruction);
                                    },
                                    opcode::LOD => {
                                        self.lod(next_instruction);
                                    },
                                    opcode::ADD => {
                                        self.add(next_instruction);
                                    },
                                    opcode::SUB => {
                                        self.sub(next_instruction);
                                    },
                                    opcode::MUL => {
                                        self.mul(next_instruction);
                                    },
                                    opcode::DIV => {
                                        self.div(next_instruction);
                                    },
                                    opcode::PRR => {
                                        self.prr(next_instruction);
                                    },
                                    opcode::STO => {
                                        self.store_type(next_instruction);
                                    },
                                    opcode::PRI => {
                                        self.pri(next_instruction);
                                    },
                                    opcode::CALL => {
                                        self.call(next_instruction);
                                    },
                                    opcode::RET => {
                                        self.ret(next_instruction);
                                    },
                                    opcode::LIST => {
                                        self.store_list(next_instruction);
                                    },
                                    opcode::FORL => {
                                        self.for_loop(next_instruction);
                                    },
                                    opcode::JMPB => {
                                        self.jmpb(next_instruction);
                                    },
                                    opcode::NXT => {
                                        self.next(next_instruction);
                                    },
                                    _ => {
                                        println!("Unrecognized instruction: {:?}", next_instruction);
                                    },
                                }
                                self.pc = self.pc + 1;
                            },
                            None => {

                            },
                        }
                    },
                    None => {

                    },
                }
    }
    fn execute_instruction(&mut self, i: &Instruction) {
        match i.opcode.op_type {
            opcode::PRL => {
                self.prl(i);
            },
            opcode::LOD => {
                self.lod(i);
            },
            opcode::ADD => {
                self.add(i);
            },
            opcode::SUB => {
                self.sub(i);
            },
            opcode::MUL => {
                self.mul(i);
            },
            opcode::DIV => {
                self.div(i);
            },
            opcode::PRR => {
                self.prr(i);
            },
            opcode::STO => {
                self.store_type(i);
            },
            opcode::PRI => {
                self.pri(i);
            },
            opcode::CALL => {
                self.call(i);
            },
            opcode::RET => {
                self.ret(i);
            },
            opcode::LIST => {
                self.store_list(i);
            },
            opcode::FORL => {
                self.for_loop(i);
            },
            opcode::JMPB => {
                self.jmpb(i);
            },
            opcode::NXT => {
                self.next(i);
            },
            _ => {
                println!("Unrecognized instruction: {:?}", i);
            },
        }
        self.pc = self.pc + 1;
    }

    /// JMPB causes the PC to be decremented
    fn jmpb(&mut self, ins: &Instruction) {
        let amount = ins.operands[0].value.clone();
        println!("Jumping backward by: {:?}", amount);
        self.pc = self.pc - amount as usize;
    }

    /// FORL handles iterating over a collection and executing code
    fn for_loop(&mut self, ins: &Instruction) {
        println!("Instructions are: {:?}", ins);
        self.looping = true;
        // First we need to get the collection
        let collection_name = ins.operands[0].identifier.clone().unwrap();
        let iterator_name = ins.operands[1].identifier.clone().unwrap();
        let collection_address = self.symbols.get(&collection_name).unwrap().clone();
        let mut collection = self.vm_heap.get_mut(collection_address).unwrap().as_collection().unwrap();
        let next = collection.Next();
        if next.is_some() {
            self.objects.insert(iterator_name, next.unwrap());
        }
    }

    /// NXT increments the current iterator
    fn next(&mut self, ins: &Instruction) {
        println!("Executing next");
        let collection_name = ins.operands[0].identifier.clone().unwrap();
        let iterator_name = ins.operands[1].identifier.clone().unwrap();
        let collection_address = self.symbols.get(&collection_name).unwrap().clone();
        let mut collection = self.vm_heap.get_mut(collection_address).unwrap().as_collection().unwrap();
        let next = collection.Next();
        if next.is_some() {
            self.objects.insert(iterator_name, collection.Next().unwrap());
        } else {
            self.pc = self.pc + 2;
            self.looping = false;
            // TODO: Cleanup scopes?
        }
    }

    /// LIST creates a list and its elements and stores it in the heap
    fn store_list(&mut self, ins: &Instruction) {
        let mut new_list = Box::new(list::List::new());
        for elem in &ins.operands[0].strings {
            let obj_type = object::determine_type(elem.clone().to_string());
            let mut o: Box<Object>;

            match obj_type {
                object::ObjectType::Integer => {
                    o = Box::new(integer::Integer::new(elem.parse::<i64>().unwrap()));
                },
                object::ObjectType::Boolean => {
                    if elem == "True" {
                        o = Box::new(boolean::Boolean::new(true));
                    } else {
                        o = Box::new(boolean::Boolean::new(false));
                    }
                },
                object::ObjectType::Function => {
                    o = Box::new(null::Null::new());
                },
                object::ObjectType::List => {
                    o = Box::new(null::Null::new());
                },
                object::ObjectType::IRString => {
                    o = Box::new(ir_string::IRString::new(elem.clone()));
                },
                object::ObjectType::Identifier => {
                    o = Box::new(null::Null::new());
                },
                object::ObjectType::Null => {
                    o = Box::new(null::Null::new());
                },
                object::ObjectType::Float => {
                    o = Box::new(float::Float::new(elem.parse::<f64>().unwrap()));
                },
                _ => {
                    o = Box::new(null::Null::new());
                },
            }
            new_list.append(o);
        }

        let list_identifier = ins.operands[0].identifier.clone().unwrap();
        if self.symbols.contains_key(&list_identifier) {
            self.vm_heap.store_existing(new_list, *(self.symbols.get(&list_identifier).unwrap()));
        } else {
            let address = self.vm_heap.store(new_list);
            self.symbols.insert(list_identifier, address);
        }
        println!("Stored list in heap");
    }

    /// ADD adds two registers together and stores the result in the first
    fn add(&mut self, ins: &Instruction) {
        let v1: Option<i64>;
        let v2: Option<i64>;
        let result: i64;

        let left = ins.operands[0].clone_identifier();
        if left.is_none() {
            v1 = Some(self.registers[ins.operands[0].register].value.unwrap());
        } else {
            v1 = self.get_i64_from_heap(left.unwrap());
        }

        let right = ins.operands[1].clone_identifier();
        if right.is_none() {
            v2 = Some(self.registers[ins.operands[1].register].value.unwrap());
        } else {
            v2 = self.get_i64_from_heap(right.unwrap());
        }
        println!("Adding: {:?} {:?}", v1, v2);
        if v1.is_none() || v2.is_none() {
            return;
        } else {
            result = v1.unwrap() + v2.unwrap();
        }


        let result_address = ins.operands[2].register;
        self.registers[result_address].value = Some(result);
        self.lr.value = Some(result);
    }

    /// SUB subtracts two registers from each other and stores the result in the first
    fn sub(&mut self, ins: &Instruction) {
        let r1 = ins.operands[0].register;
        let r2 = ins.operands[1].register;
        let result = self.registers[r1].value.unwrap() - self.registers[r2].value.unwrap();
        self.registers[r1].value = Some(result);
    }

    /// MUL multiplies two registers together and stores the result in the first
    fn mul(&mut self, ins: &Instruction) {
        let r1 = ins.operands[0].register;
        let r2 = ins.operands[1].register;
        let result = self.registers[r1].value.unwrap() * self.registers[r2].value.unwrap();
        self.registers[r1].value = Some(result);
    }

    /// DIV divides two registers and stores the result in the first
    fn div(&mut self, ins: &Instruction) {
        let r1 = ins.operands[0].register;
        let r2 = ins.operands[1].register;
        let result = Some(self.registers[r1].value.unwrap() / self.registers[r2].value.unwrap());
        self.registers[r1].value = result;
    }

    /// pri prints the value of an identifier
    fn pri(&mut self, ins: &Instruction) {
        let address = self.symbols.get(&(ins.operands[0].identifier.clone().unwrap()));
        let object = self.vm_heap.get(*(address.unwrap())).unwrap();
        println!("{:?}", object.Text());
    }

    fn call(&mut self, ins: &Instruction) {
        let mut instructions: Vec<Instruction>;
        let name = self.program.as_mut().unwrap().current_function.clone();
        let fp = FunctionPointer::new(name.clone(), self.pc);
        self.program.as_mut().unwrap().call_stack.push(fp);
        self.program.as_mut().unwrap().get_function(name.clone()).unwrap().set_parent(name.clone());
        self.program.as_mut().unwrap().current_function = ins.operands[0].identifier.clone().unwrap().clone();
        let function_name = ins.operands[0].identifier.clone().unwrap();

        instructions = self.program.as_mut().unwrap().get_function_instructions(function_name).unwrap().clone();

        for i in instructions {
            self.execute_instruction(&i);
        }
    }

    fn ret(&mut self, ins: &Instruction) {
        match self.program {
            Some(ref mut p) => {
                let ret_address = p.call_stack.pop().unwrap();
                p.current_function = ret_address.identifier;
                self.pc = ret_address.fc;
            },
            None => {},

        }

    }

    /// Attempts to store the value in a register on the heap using a type
    fn store_type(&mut self, ins: &Instruction) {

        let r = ins.operands[0].register;
        let t = compiler::symbol::int_to_symbol_type(ins.operands[0].value as usize);
        let i = ins.operands[0].identifier.clone();
        match t {
            compiler::symbol::SymbolType::Integer => {
                self.load_int(r, t, i.unwrap());
            },
            compiler::symbol::SymbolType::Boolean => {
                self.load_boolean(r as i64, ins.operands[0].value as i64, i.unwrap());
            },
            compiler::symbol::SymbolType::Function => {
                self.load_function(ins);
            },
            compiler::symbol::SymbolType::List => {
                // TODO: This should never be executed
                self.store_list(ins);
            },
            compiler::symbol::SymbolType::IRString => {
                self.load_string(ins);
            },
            compiler::symbol::SymbolType::Invalid => {
                println!("Invalid symbol type");
            },
        }
        self.lr.value = None;
    }

    /// Creates and loads an Integer into the heap
    fn load_int(&mut self, r1: usize, st: compiler::symbol::SymbolType, identifier: String) {
        let value = self.registers[r1].value;
        let o = Box::new(integer::Integer::new(value.unwrap()));
        if self.symbols.contains_key(&identifier) {
            self.vm_heap.store_existing(o, *(self.symbols.get(&identifier).unwrap()));
        } else {
            let address = self.vm_heap.store(o);
            self.symbols.insert(identifier, address);
        }
    }

    /// Creates and loads an IRString
    fn load_string(&mut self, ins: &Instruction) {

    }
    
    /// Creates and loads an Boolean into the heap
    fn load_boolean(&mut self, r1: i64, value: i64, identifier: String) {
        let boolean_object: Box<vm::types::boolean::Boolean>;
        if r1 == 0 {
            boolean_object = Box::new(vm::types::boolean::Boolean::new(false));
        } else {
            boolean_object = Box::new(vm::types::boolean::Boolean::new(true));
        }

        if self.symbols.contains_key(&identifier) {
            self.vm_heap.store_existing(boolean_object, *(self.symbols.get(&identifier).unwrap()));
        } else {
            let address = self.vm_heap.store(boolean_object);
            self.symbols.insert(identifier, address);
        }
    }

    fn load_function(&mut self, ins: &Instruction) {
        // When loading a function, the Instruction has the following information:
        // The first operand ID has the function name
        let function_name = ins.operands[0].identifier.clone().unwrap();
        // The first operand value has the number of arguments
        let num_args = ins.operands[0].register.clone();
        let mut args = Vec::<String>::new();
        for a in &ins.operands[1 ..] {
            match a.identifier {
                Some(ref id) => {
                    args.push(a.identifier.clone().unwrap());
                },
                None => {},
            }
        }

        let start_instruction = ins.operands[1].register;
        let end_instruction = ins.operands[1].value;
        let function_header = vm::types::function::FunctionHeader::new(args);
        let function_body = vm::types::function::FunctionBody::new(start_instruction as i64, end_instruction);
        let function = vm::types::function::Function::new(function_name.clone(), function_header, function_body);
        if self.symbols.contains_key(&function_name) {
            self.vm_heap.store_existing(Box::new(function), *(self.symbols.get(&function_name).unwrap()));
        } else {
            let address = self.vm_heap.store(Box::new(function));
            self.symbols.insert(function_name, address);
        }
    }

}

#[cfg(test)]
mod tests {
    use super::*;

    fn load_test_values(vm: &mut VM) {
        vm.add_register();
        vm.add_register();
        vm.add_register();
        // Load in some numbers
        let mut ins = Instruction::new(opcode::LOD).unwrap();
        let operand1 = Operand::new(0, 10);
        ins.add_operand(operand1);
        vm.lod(&ins);
        let mut ins = Instruction::new(opcode::LOD).unwrap();
        let operand1 = Operand::new(1, 5);
        ins.add_operand(operand1);
        vm.lod(&ins);
    }

    #[test]
    fn test_add_register() {
        let mut vm = VM::new();
        vm.add_register();
        // TODO: Need to set a const for number of registers or something
        assert_eq!(REGISTERS+1, vm.registers.len());
    }

    #[test]
    fn test_lod() {
        let mut vm = VM::new();
        vm.add_register();
        vm.add_register();
        // Load in some numbers
        let mut ins = Instruction::new(opcode::LOD).unwrap();
        let operand1 = Operand::new(0, 10);
        ins.add_operand(operand1);
        vm.lod(&ins);
        assert_eq!(vm.registers[0].value.unwrap(), 10);
    }

    #[test]
    fn test_add() {
        let mut vm = VM::new();
        load_test_values(&mut vm);
        let operand1 = Operand::new(0, 1);
        let operand2 = Operand::new(1, 2);
        let operand3 = Operand::new(2, 0);
        let mut ins = Instruction::new(opcode::ADD).unwrap();
        ins.add_operand(operand1);
        ins.add_operand(operand2);
        ins.add_operand(operand3);
        vm.add(&ins);
        assert_eq!(vm.registers[2].value.unwrap(), 15);
    }

    #[test]
    fn test_sub() {
        let mut vm = VM::new();
        load_test_values(&mut vm);
        let operand3 = Operand::new(0, 0);
        let operand4 = Operand::new(1, 0);
        let mut ins = Instruction::new(opcode::SUB).unwrap();
        ins.add_operand(operand3);
        ins.add_operand(operand4);
        vm.sub(&ins);
        assert_eq!(vm.registers[0].value.unwrap(), 5);
    }

    #[test]
    fn test_mul() {
        let mut vm = VM::new();
        load_test_values(&mut vm);
        let operand3 = Operand::new(0, 0);
        let operand4 = Operand::new(1, 0);
        let mut ins = Instruction::new(opcode::MUL).unwrap();
        ins.add_operand(operand3);
        ins.add_operand(operand4);
        vm.mul(&ins);
        assert_eq!(vm.registers[0].value.unwrap(), 50);
    }

    #[test]
    fn test_div() {
        let mut vm = VM::new();
        load_test_values(&mut vm);
        let operand3 = Operand::new(0, 0);
        let operand4 = Operand::new(1, 0);
        let mut ins = Instruction::new(opcode::DIV).unwrap();
        ins.add_operand(operand3);
        ins.add_operand(operand4);
        vm.div(&ins);
        assert_eq!(vm.registers[0].value.unwrap(), 2);
    }

    #[test]
    fn test_prl() {
        let mut vm = VM::new();
        let mut ins = Instruction::new(opcode::PRL).unwrap();
        let operand1 = Operand::new(1, 10);
        ins.add_operand(operand1);
        vm.prl(&ins);
    }
}
