use vm::cpu::register::Register;
use vm::memory::ram::ram::RAMStore;
const NUM_REGISTERS: usize = 32;


/// CPU executes instructions and stores data in registers
pub struct CPU {
    registers: [Register; NUM_REGISTERS],
    hi: Register,
    lo: Register,
    memory: RAMStore,
}

impl CPU {
    /// Creates and returns a new CPU
    pub fn new() -> CPU {
        CPU {
            registers: [Register::new(); NUM_REGISTERS],
            memory: RAMStore::new(),
            hi: Register::new(),
            lo: Register::new(),
        }
    }

    fn add(&mut self, r1: usize, r2: usize, r3: usize) {
        self.registers[r3].data = self.registers[r1].data + self.registers[r2].data;
    }

    fn sub(&mut self, r1: usize, r2: usize, r3: usize) {
        self.registers[r3].data = self.registers[r1].data - self.registers[r2].data;
    }

    fn mul(&mut self, r1: usize, r2: usize, r3: usize) {
        self.registers[r3].data = self.registers[r1].data * self.registers[r2].data;
    }

    fn div(&mut self, r1: usize, r2: usize) {
        self.lo.data = self.registers[r1].data / self.registers[r2].data
    }

    /// loads a 32-bit integer into a register from RAM
    fn lw(&mut self, r1: usize, source: usize) {
        self.registers[r1].data = self.memory.read_word(source) as u64;
    }

    /// loads a 64-bit integer into a register from RAM
    fn ldw(&mut self, r1: usize, source: usize) {
        self.registers[r1].data = self.memory.read_dword(source);
    }

    /// Loads a double word into RAM at the destination
    fn sdw(&mut self, r1: usize, destination: usize) {
        self.memory.store_dword(destination, self.registers[r1].data);
    }

    /// Loads an immediate value into a register
    fn li(&mut self, r1: usize, value: u64) {
        self.registers[r1].data = value;
    }
}