#[derive(Copy, Clone, Debug)]
/// A Register represents a CPU register
pub struct Register {
    pub data: u64,
}

impl Register {
    /// Creates and returns a new Register
    pub fn new() -> Register {
        Register {
            data: 0,
        }
    }
}