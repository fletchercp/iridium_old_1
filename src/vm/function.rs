use vm::register::Register;
use vm::memory::heap::VMHeap;
use vm::instruction::Instruction;
use std::collections::HashMap;

#[derive(Debug, Clone)]
pub struct Function {
    pub instructions: Vec<Instruction>,
    symbols: HashMap<String, bool>,
    fc: usize,
    parent: Option<String>,
}

impl Function {
    pub fn new() -> Function {
        Function {
            instructions: Vec::new(),
            fc: 0,
            symbols: HashMap::new(),
            parent: None,
        }
    }

    pub fn add_symbol(&mut self, s: String) {
        self.symbols.insert(s, true);
    }

    pub fn set_parent(&mut self, s: String) {
        self.parent = Some(s);
    }

    pub fn get_symbol(&mut self, s: String) -> bool {
        let result = self.symbols.get(&s);
        match result {
            Some(r) => {
                true
            },
            None => {
                false
            }
        }
    }

    pub fn add_instruction(&mut self, i: Instruction) {
        self.instructions.push(i);
    }

    pub fn get_instruction(&mut self, idx: usize) -> &Instruction {
        &self.instructions[idx]
    }

    pub fn add_instructions(&mut self, i: &mut Vec<Instruction>) {
        self.instructions.append(i);
    }

    pub fn len(&self) -> usize {
        self.instructions.len()
    }
}