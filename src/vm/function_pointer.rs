#[derive(Clone, Debug)]
/// This is used to store information about where we leave when we encounter a CALL instruction
pub struct FunctionPointer {
    // Name of the function
    pub identifier: String,
    // Instruction in the function
    pub fc: usize,
}

impl FunctionPointer {
    /// Creates and returns a new FunctionPointer
    pub fn new(id: String, fc: usize) -> FunctionPointer {
        FunctionPointer {
            identifier: id,
            fc: fc,
        }
    }
}