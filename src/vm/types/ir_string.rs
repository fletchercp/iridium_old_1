use vm::types::object;
use vm::types::object::Object;
use vm::types::collection::Collection;
/// IRString is a string
pub struct IRString {
    obj_type: object::ObjectType,
    value: String,
}

impl IRString {
    pub fn new(value: String) -> IRString {
        IRString {
            obj_type: object::ObjectType::IRString,
            value: value,
        }
    }
}

impl Object for IRString {
    fn GetType(&self) -> object::ObjectType {
        self.obj_type.clone()
    }

    fn Text(&self) -> String {
        self.value.clone()
    }

    fn as_collection(&mut self) -> Option<&mut Collection> {
        None
    }
}