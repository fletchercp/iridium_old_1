use vm::types::object;
use vm::types::object::Object;
use vm::types::collection::Collection;

/// Float is a float data type
pub struct Float {
    obj_type: object::ObjectType,
    value: f64,
}

impl Float {
    pub fn new(value: f64) -> Float {
        Float {
            obj_type: object::ObjectType::Float,
            value: value,
        }
    }
}

impl Object for Float {
    fn GetType(&self) -> object::ObjectType {
        self.obj_type.clone()
    }

    fn Text(&self) -> String {
        self.value.to_string()
    }
    fn as_collection(&mut self) -> Option<&mut Collection> {
        None
    }
}