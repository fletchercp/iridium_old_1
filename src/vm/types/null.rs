use vm::types::object;
use vm::types::object::Object;
use vm::types::collection::Collection;
/// Null is a null value
pub struct Null {
    obj_type: object::ObjectType,
}

impl Null {
    pub fn new() -> Null {
        Null {
            obj_type: object::ObjectType::Null,
        }
    }
}

impl Object for Null {
    fn GetType(&self) -> object::ObjectType {
        self.obj_type.clone()
    }

    fn Text(&self) -> String {
        String::from("Null")
    }

    fn as_collection(&mut self) -> Option<&mut Collection> {
        None
    }
}