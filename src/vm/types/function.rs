use vm::instruction::Instruction;
use vm::types::object::Object;
use vm::types::object::ObjectType;
use vm::types::collection::Collection;
/// Function is a chunk of code that can be called
pub struct Function {
    pub identifier: String,
    pub header: FunctionHeader,
    pub body: FunctionBody,
}

impl Function {
    pub fn new(identifier: String, h: FunctionHeader, b: FunctionBody) -> Function {
        Function{
            identifier: identifier,
            header: h,
            body: b,
        }
    }
}

impl Object for Function {
    fn GetType(&self) -> ObjectType {
        ObjectType::Function
    }
    
    fn Text(&self) -> String {
        let mut s = String::from("Name: ");
        s = s + &self.identifier + " Args: ";
        for a in &self.header.args {
            let tmp = a.clone() + &", ".to_string();
            s = s + &tmp;
        }
        s
    }

    fn as_collection(&mut self) -> Option<&mut Collection> {
        None
    }
}

/// FunctionHeader is the function signature
/// For exampe: def x(a, b, c)
pub struct FunctionHeader {
    pub args: Vec<String>,
}

impl FunctionHeader {
    pub fn new(args: Vec<String>) -> FunctionHeader {
        FunctionHeader {
            args: args,
        }
    }
}

/// FunctionBody is the body of the function. It contains a Vector of Instructions that compromise the function
pub struct FunctionBody {
    start_instruction: i64,
    end_instruction: i64,
}

impl FunctionBody {
    pub fn new(s: i64, e: i64) -> FunctionBody {
        FunctionBody {
            start_instruction: s,
            end_instruction: e,
        }
    }
}

