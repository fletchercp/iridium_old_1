use regex::Regex;
use vm::types::collection::Collection;
/// Object is the trait all data types must fulfill
pub trait Object {
    fn GetType(&self) -> ObjectType;
    fn Text(&self) -> String;
    fn as_collection(&mut self) -> Option<&mut Collection>;
}

#[derive(Clone, Debug)]
pub enum ObjectType {
    Null,
    Boolean,
    Integer,
    Float,
    Function,
    IRString,
    List,
    Identifier,
    Invalid,
}

/// Attempts to determine a type from a string
pub fn determine_type(s: String) -> ObjectType {
    // A string will be enclosed in ' '
    // Numbers are, well, numbers
    // Null is None
    // A variable will be an identifier with no '

    // First check if there is a ' at beginning and end
    if s.starts_with("'") && s.ends_with("'") {
        return ObjectType::IRString;
    }

    // Check if its None
    if s == "None" {
        return ObjectType::Null;
    }

    if s == "True" || s == "False" {
        return ObjectType::Boolean;
    }

    let is_int = s.parse::<i64>();
    if is_int.is_ok() == true {
        return ObjectType::Integer;
    }

    let is_float = s.parse::<f64>();
    if is_float.is_ok() == true {
        return ObjectType::Float;
    }

    let re = Regex::new(r"[A-Za-z_]+").unwrap();
    if re.is_match(&s) {
        return ObjectType::Identifier;
    }

    return ObjectType::Invalid;
}