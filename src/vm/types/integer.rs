use vm::types::object;
use vm::types::object::Object;
use vm::types::collection::Collection;
/// Integer is an integer data type
pub struct Integer {
    obj_type: object::ObjectType,
    value: i64,
}

impl Integer {
    pub fn new(value: i64) -> Integer {
        Integer {
            obj_type: object::ObjectType::Integer,
            value: value,
        }
    }
}

impl Object for Integer {
    fn GetType(&self) -> object::ObjectType {
        self.obj_type.clone()
    }

    fn Text(&self) -> String {
        self.value.to_string()
    }

    fn as_collection(&mut self) -> Option<&mut Collection> {
        None
    }
}