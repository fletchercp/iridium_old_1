use vm::types::object::*;
use vm::types;
use std::sync::{Arc, RwLock};
use vm::types::collection::Collection;
/// List is a collection of other Objects
pub struct List {
    obj_type: ObjectType,
    items: Vec<Arc<RwLock<Box<Object>>>>,
    current_index: usize,
}

impl List {
    /// Creates and returns a new list
    pub fn new() -> List {
        List {
            obj_type: ObjectType::List,
            items: Vec::new(),
            current_index: 0,
        }
    }

    /// Appends an object to the end of a list
    pub fn append(&mut self, o: Box<Object>) {
        self.items.push(Arc::new(RwLock::new(o)));
    }

    /// Inserts an object at a specific index
    pub fn insert(&mut self, o: Box<Object>, idx: usize) {
        self.items.insert(idx, Arc::new(RwLock::new(o)));
    }

    /// Counts the number of items in a list
    pub fn len(&self) -> usize {
        self.items.len()
    }
}

impl Collection for List {
    fn Next(&mut self) -> Option<Arc<RwLock<Box<Object>>>> {
        if self.current_index >= self.items.len() {
            return None;
        }
        let current = self.items[self.current_index].clone();
        let next_index = self.current_index + 1;
        self.current_index = next_index;
        Some(current)
    }

    fn Current(&mut self) -> Arc<RwLock<Box<Object>>> {
        self.items[self.current_index].clone()
    }
}

impl Object for List {
    fn GetType(&self) -> ObjectType {
        self.obj_type.clone()
    }

    fn Text(&self) -> String {
        let mut output = String::from("[");
        for elem in &self.items {
            output.push_str(&elem.read().unwrap().Text());
            output.push_str(", ");
        }
        output.push_str("]");
        output
    }

    fn as_collection(&mut self) -> Option<&mut Collection> {
        Some(self)
    }
}



#[cfg(test)]
mod tests {
    use super::*;
    use vm::types::integer::Integer;

    #[test]
    fn add_item_to_list() {
        let mut test_strings = Vec::<String>::new();
        test_strings.push(String::from("test_list"));
        let mut test_list = List::new(test_strings);
        let test_obj = Integer::new(10);
        test_list.append(Box::new(test_obj));
        assert_eq!(test_list.len(), 1);
    }

    #[test]
    fn insert_item_into_list() {
        let mut test_strings = Vec::<String>::new();
        test_strings.push(String::from("test_list"));
        let mut test_list = List::new(test_strings);
        let test_obj = Integer::new(10);
        test_list.append(Box::new(test_obj));
        let test_obj_two = Integer::new(5);
        test_list.insert(Box::new(test_obj_two), 0);
        assert_eq!(test_list.len(), 2);
    }

    #[test]
    fn count_items_in_list() {
        let mut test_strings = Vec::<String>::new();
        test_strings.push(String::from("test_list"));
        let mut test_list = List::new(test_strings);
        assert_eq!(test_list.len(), 0);
    }
}