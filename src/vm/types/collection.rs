use vm::types::object;
use vm::types::object::Object;
use std::sync::{Arc, RwLock};

pub trait Collection {
    fn Next(&mut self) -> Option<Arc<RwLock<Box<Object>>>>;
    fn Current(&mut self) -> Arc<RwLock<Box<Object>>>;
}