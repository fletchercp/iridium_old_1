use vm::types::object;
use vm::types::object::Object;
use vm::types::collection::Collection;
/// Boolean is true or false
pub struct Boolean {
    obj_type: object::ObjectType,
    value: bool,
}

impl Boolean {
    pub fn new(v: bool) -> Boolean {
        Boolean {
            obj_type: object::ObjectType::Boolean,
            value: v,
        }
    }
}

impl Object for Boolean {
    fn GetType(&self) -> object::ObjectType {
        self.obj_type.clone()
    }

    fn Text(&self) -> String {
        if self.value == true {
            String::from("True")
        } else {
            String::from("False")
        }
    }
    fn as_collection(&mut self) -> Option<&mut Collection> {
        None
    }
}