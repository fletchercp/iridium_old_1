use vm::opcode;
use vm::opcode::Opcode;
use vm::operand::Operand;

#[derive(Debug, Clone)]
/// Instruction is a combination of Opcode and Operand that is executed by the VM
pub struct Instruction {
    pub opcode: Opcode,
    pub operands: Vec<Operand>,
}

impl Instruction {
    /// Creates and returns a new Instruction
    pub fn new(opcode: usize) -> Option<Instruction> {
        let o = Opcode::new(opcode);
        match o {
            None => {
                return None;
            }
            Some(_) => {
                let new_instruction = Instruction {
                    opcode: o.unwrap(),
                    operands: Vec::new(),
                };
                return Some(new_instruction);
            }
        }
    }

    pub fn new_from_lod(value: i64, register: usize) -> Instruction {
        let o = Opcode::new(opcode::LOD).unwrap();
        let op1 = Operand::new(register, value);
        let mut new_instruction = Instruction{
            opcode: o,
            operands: Vec::new(),
        };
        new_instruction.add_operand(op1);
        new_instruction
    }

    pub fn new_from_add(r1: usize, r2: usize, r3: usize) -> Instruction {
        let o = Opcode::new(opcode::ADD).unwrap();
        let op1 = Operand::new(r1, 0);
        let op2 = Operand::new(r2, 0);
        let op3 = Operand::new(r3, 0);
        let mut new_instruction = Instruction{
            opcode: o,
            operands: Vec::new(),
        };
        new_instruction.add_operand(op1);
        new_instruction.add_operand(op2);
        new_instruction.add_operand(op3);
        new_instruction
    }

    pub fn new_from_sub(r1: usize, r2: usize, r3: usize) -> Instruction {
        let o = Opcode::new(opcode::SUB).unwrap();
        let op1 = Operand::new(r1, 0);
        let op2 = Operand::new(r2, 0);
        let op3 = Operand::new(r3, 0);
        let mut new_instruction = Instruction{
            opcode: o,
            operands: Vec::new(),
        };
        new_instruction.add_operand(op1);
        new_instruction.add_operand(op2);
        new_instruction.add_operand(op3);
        new_instruction
    }

    pub fn new_from_mul(r1: usize, r2: usize, r3: usize) -> Instruction {
        let o = Opcode::new(opcode::MUL).unwrap();
        let op1 = Operand::new(r1, 0);
        let op2 = Operand::new(r2, 0);
        let op3 = Operand::new(r3, 0);
        let mut new_instruction = Instruction{
            opcode: o,
            operands: Vec::new(),
        };
        new_instruction.add_operand(op1);
        new_instruction.add_operand(op2);
        new_instruction.add_operand(op3);
        new_instruction
    }
    pub fn new_from_div(r1: usize, r2: usize, r3: usize) -> Instruction {
        let o = Opcode::new(opcode::DIV).unwrap();
        let op1 = Operand::new(r1, 0);
        let op2 = Operand::new(r2, 0);
        let op3 = Operand::new(r3, 0);
        let mut new_instruction = Instruction{
            opcode: o,
            operands: Vec::new(),
        };
        new_instruction.add_operand(op1);
        new_instruction.add_operand(op2);
        new_instruction.add_operand(op3);
        new_instruction
    }

    pub fn new_from_prr(r1: usize) -> Instruction {
        let o = Opcode::new(opcode::PRR).unwrap();
        let op1 = Operand::new(r1, 0);
        let mut new_instruction = Instruction {
            opcode: o,
            operands: Vec::new(),
        };
        new_instruction.add_operand(op1);
        new_instruction
    }

    pub fn new_from_sto(r1: usize, symbol_type: usize, identifier: String) -> Instruction {
        let o = Opcode::new(opcode::STO).unwrap();
        let mut op1 = Operand::new(r1, symbol_type as i64);
        op1.identifier = Some(identifier);
        let mut new_instruction = Instruction {
            opcode: o,
            operands: Vec::new(),
        };
        new_instruction.add_operand(op1);
        new_instruction
    }

    pub fn new_from_addh(op1: Operand, op2: Operand, op3: Operand) -> Instruction {
        let o = Opcode::new(opcode::ADDH).unwrap();
        let mut new_instruction = Instruction {
            opcode: o,
            operands: Vec::new(),
        };
        new_instruction.add_operand(op1);
        new_instruction.add_operand(op2);
        new_instruction.add_operand(op3);
        new_instruction
    }

    pub fn new_from_call(operands: Vec<Operand>) -> Instruction {
        let o = Opcode::new(opcode::CALL).unwrap();
        Instruction {
            opcode: o,
            operands: operands,
        }
    }

    pub fn new_from_list(operands: Vec<Operand>) -> Instruction {
        let o = Opcode::new(opcode::LIST).unwrap();
        Instruction {
            opcode: o,
            operands: operands,
        }
    }

    pub fn new_from_forl(operand: Operand) -> Instruction {
        let o = Opcode::new(opcode::FORL).unwrap();
        let mut new_instruction = Instruction {
            opcode: o,
            operands: Vec::new(),
        };
        new_instruction
    }

    /// Adds an Operand to an instruction
    pub fn add_operand(&mut self, o: Operand) {
        self.operands.push(o);
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_instruction() {
        let ins = Instruction::new(opcode::ADD).unwrap();
        assert_eq!(ins.opcode.op_type, opcode::ADD);
    }

    #[test]
    fn test_add_operand() {
        let mut ins = Instruction::new(opcode::ADD).unwrap();
        let operand = Operand::new(5, 10);
        ins.add_operand(operand);
        assert_eq!(ins.operands.len(), 1);
    }
}