extern crate byteorder;
use std::io::Cursor;
use vm::memory::ram::ram::byteorder::ByteOrder;
const STARTING_RAM: usize = 1024 * 1024;
/// RAM represents the VMs RAM
pub struct RAMStore {
    data: Vec<u8>,
}

impl RAMStore {
    pub fn new() -> RAMStore {
        RAMStore {
            data: Vec::with_capacity(STARTING_RAM),
        }
    }

    /// Reads num_bytes starting at offset and returns them as a slice
    pub fn read_word(&self, offset: usize) -> u32 {
        //let mut rdr = Cursor::new(&self.data[offset..offset+4])
        //rdr.read_u32::<LittleEndian>().unwrap()
        byteorder::LittleEndian::read_u32(&self.data[offset..offset+4])
    }

    /// Reads num_bytes starting at offset and returns them as a slice
    pub fn read_dword(&self, offset: usize) -> u64 {
        byteorder::LittleEndian::read_u64(&self.data[offset..offset+8])
    }

    /// Stores a 64-bit value in RAM at an offset
    pub fn store_dword(&mut self, destination: usize, value: u64) {
        let mut buf = &mut self.data[destination..destination+8];
        byteorder::LittleEndian::write_u64(buf, value);
    }

    /// Writes bytes to the RAM starting at offset
    pub fn write(&mut self, offset: usize, bytes: &[u8]) {
        let mut idx = offset;
        for b in bytes {
            self.data[idx] = b.clone();
            idx = idx + 1;
        }
    }
}