use std::collections::HashMap;
use vm::types::object::Object;

/// Heap represents the heap storage of a Process
pub struct VMHeap {
    pub storage: HashMap<usize, Box<Object>>,
    next_id: usize,
}

impl VMHeap {
    pub fn new() -> VMHeap {
        VMHeap {
            storage: HashMap::new(),
            next_id: 0,
        }
    }

    pub fn store(&mut self, o: Box<Object>) -> usize {
        let id = self.next_id;
        self.next_id = self.next_id + 1;
        self.storage.insert(id, o);
        id
    }

    pub fn store_existing(&mut self, o: Box<Object>, address: usize) {
        self.storage.insert(address, o);
    }


    pub fn get_mut(&mut self, id: usize) -> Option<&mut Box<Object>> {
        self.storage.get_mut(&id)
    }

    pub fn get(&self, id: usize) -> Option<&Box<Object>> {
        self.storage.get(&id)
    }
}