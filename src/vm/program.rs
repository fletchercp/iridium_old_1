use vm::instruction::Instruction;
use vm::opcode;
use std::collections::HashMap;
use vm::function_pointer::FunctionPointer;
use vm::function::Function;
#[derive(Debug, Clone)]
/// Program stores a program's bytecode and various other information
pub struct Program {
    pub name: String,
    pub functions: HashMap<String, Function>,
    pub current_function: String,
    pub call_stack: Vec<FunctionPointer>,
}

impl Program {
    /// Creates a new program
    pub fn new(name: String) -> Program {
        let mut new_program = Program {
            name: name,
            functions: HashMap::new(),
            current_function: String::from("main"),
            call_stack: Vec::new(),
        };
        new_program.functions.insert(String::from("main"), Function::new());
        new_program
    }

    /// Adds an Instruction to the end of the Program's list of Instructions
    pub fn add_instruction(&mut self, function_name: String, i: Instruction) {
        let mut function = self.functions.get_mut(&function_name);
        match function {
            Some(f) => {
                f.instructions.insert(0, i);
            },
            None => {
                // TODO: Something here...
            },
        }
    }

    pub fn add_function(&mut self, function_name: String) {
        self.functions.insert(function_name, Function::new());
    }

    pub fn get_function_instructions(&mut self, function_name: String) -> Option<&Vec<Instruction>> {
        Some(&mut self.functions.get_mut(&function_name).unwrap().instructions)
    }

    pub fn get_function(&mut self, function_name: String) -> Option<&mut Function> {
        Some(self.functions.get_mut(&function_name).unwrap())
    }

    pub fn add_instruction_to_current_function(&mut self, i: Instruction) {
        let mut function = self.functions.get_mut(&self.current_function);
        match function {
            Some(f) => {
                f.instructions.insert(0, i);
            },
            None => {
                // TODO: Something here...
            },
        }
    }

    pub fn add_instructions_to_current_function(&mut self, i: &mut Vec<Instruction>) {
        let mut function = self.functions.get_mut(&self.current_function);
        match function {
            Some(f) => {
                f.instructions.append(i);
            },
            None => {
                // TODO: Something here...
            },
        }
    }

    pub fn add_instruction_to_function(&mut self, function_name: String, i: Instruction) {
        let mut function_vec = self.functions.get_mut(&function_name);
        match function_vec {
            Some(v) => {
                v.instructions.push(i);
            },
            None => {
                // TODO: Error here
            },
        }
    }

    pub fn add_instructions_to_function(&mut self, function_name: String, i: &mut Vec<Instruction>) {
        let mut function_vec = self.functions.get_mut(&function_name);
        match function_vec {
            Some(v) => {
                v.add_instructions(i);
            },
            None => {
                // TODO: Error here
            },
        }
    }

    pub fn print_functions(&self) {
        println!("{:?}", self.functions);
        
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_program_name() {
        let test_program = Program::new(String::from("test_program"));
        assert_eq!("test_program", test_program.name);
    }

    #[test]
    fn test_add_instruction() {
        let mut test_program = Program::new(String::from("test_program"));
        let ins = Instruction::new(opcode::ADD).unwrap();
        test_program.add_instruction(String::from("main"), ins);
        let main_function = test_program.functions.get(&String::from("main")).unwrap();
        assert_eq!(main_function.len(), 1);
    }

    #[test]
    fn test_add_function() {
        let mut test_program = Program::new(String::from("test_program"));
        test_program.add_function(String::from("test_function"));
        let f = test_program.get_function(String::from("test_function"));
        assert_eq!(f.unwrap().len(), 0);
    }
}
