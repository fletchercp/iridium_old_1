#[derive(Debug, Clone)]
/// Operand is an argument to an Opcode
pub struct Operand {
    pub register: usize,
    pub value: i64,
    pub identifier: Option<String>,
    pub strings: Vec<String>,
}

impl Operand {
    /// Creates and returns a new Operand
    pub fn new(register: usize, value: i64) -> Operand {
        let new_operand = Operand {
            register: register,
            value: value,
            identifier: None,
            strings: Vec::<String>::new(),
        };
        new_operand
    }

    pub fn set_identifier(&mut self, i: String) {
        self.identifier = Some(i);
    }

    pub fn clone_identifier(&self) -> Option<String> {
        match self.identifier {
            Some(ref i) => {
                Some(i.clone())
            },
            None => {
                None
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_operand() {
        let operand = Operand::new(5, 10);
        assert_eq!(operand.register, 5);
        assert_eq!(operand.value, 10);
    }
}
