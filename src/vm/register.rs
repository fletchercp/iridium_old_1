#[derive(Debug)]
/// Register represents a register in the VM that can store some value.
pub struct Register {
    pub id: usize,
    pub value: Option<i64>,
}

impl Register {
    /// Creates and returns a new Register
    pub fn new(id: usize, value: i64) -> Register {
        let new_register = Register {
            id: id,
            value: Some(value),
        };
        new_register
    }
}
    /*
    /// Sets the value of a register
    pub fn set_value(&mut self, value: usize) {
        self.value = value;
    }
    */

