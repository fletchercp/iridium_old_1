extern crate regex;

mod parser;
mod vm;
mod repl;
mod compiler;

use repl::repl::REPL;

fn main() {
    let mut new_repl = REPL::new();
    new_repl.start();
}
