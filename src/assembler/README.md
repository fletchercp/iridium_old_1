=================
Summary
=================

This document describes the Iridium VM architecture and instruction set.

It is based on MIPS64 Release 6 for most things, in an effort to keep things
as simple as possible for now.

=================
Byte Ordering
=================
Ordering is always Little Endian

=================
Instructions
=================
All instructions are 32 bits.

Opcodes are always the 6 left-most bits.

=================
Load and Store
=================
Main memory is accessible only via load and store instructions

lw: loads 32-bits at 